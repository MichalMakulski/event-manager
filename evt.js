function setEvtManager () {
    let events = {};
    let evtObject = {
		on (evtName, callback) {
			let isValidInput = typeof evtName === 'string' && typeof callback === 'function';
			
			if (isValidInput) {
				events[evtName] = events[evtName] || [];	
				events[evtName].push(callback);
				return;
			}
			
			console.warn(`Something's wrong with event name: "${evtName}" or function: "${callback}"`);
			
		},
		
		off (evtName) {
			if (events[evtName]) {
				delete events[evtName];
				console.info(`Event "${evtName}" successfully removed`);
			}
		},

		emit (evtName, evtData) {
			if (events[evtName]) {
				evtData = evtData || null;
				events[evtName].forEach(callback => callback(evtData));
			} else {
				console.warn(`Event: "${evtName}" not found`);
			}
		}
	}
    
    return Object.freeze(evtObject);
}